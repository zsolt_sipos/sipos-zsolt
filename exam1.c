#include <stdio.h>
#include <dirent.h>

char listfiles(char *dic[]){
    DIR *d;
    struct dirent *dir;
    d = opendir(*dic);
    if(d){
        while((dir = readdir(d)) != NULL){
            printf("%s\n", dir->d_name);
        }
        closedir(d);
    }
    return 0;
}

int main(int argc, char *argv[]){
    if(argc == 2){ listfiles(&argv[1]);} else {printf("Hasznalat: %s [eleresi utvonal]", argv[0]);}
    return 0;
}
