#include <stdio.h>
#include <dirent.h>

int main( int argc, char *argv[] )
{
    if ( argc != 2 ){
        printf( "Hasznalat: %s mappanev", argv[0] );
    }
    else {
        foldercheck();
    }
    return 0;
}

void foldercheck( int argc, char *argv[] ) {
   DIR *d;
   struct dirent *dir;
   d = opendir(argv[1]);
   if (d) {
    while ((dir = readdir(d)) != NULL) {
        printf("%s\n", dir->d_name);
    }
    closedir(d);
   }
   return 0;
}
