/*
    A program használatához, le kell debbugolni a kódot, majd parancssorból elindítani utasítás szerint!
    Ez egy alap ellenőrző, akinek az email címében van '.' azt már nem engedi tovább!
*/
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int kukac = 0;
int pont = 0;

int ValidEmail(char *email[]){
    int hossz = strlen(email[1]);
    int i;
    int szamlalo = 0;
    int check = 0;

    for(i = 0; i < hossz; i++){
        switch(toupper(email[1][i])){
            case '@':
                szamlalo++;
                break;
        }
    }

    if(szamlalo == 1){

        for(i = 0; i < hossz; i++){
            if((email[1][0] == '@') || (email[1][0] == '.') || (email[1][hossz - 1]) == '.' || (email[1][hossz - 1]) == '@' ){
                return kukac = 2, pont = 1;
            } else {
                for(i = 0; i < hossz; i++){
                    switch(toupper(email[1][i])){
                        case '.':
                            check++;
                            break;
                    }
                }
            }
        }
        if(check == 1){
            int i;
            for(i = 0; i < hossz; i++){
                switch(toupper(email[1][i])){
                    case '@':
                        kukac = i;
                        break;
                    case '.':
                        pont = i;
                        break;
               }
            }
            } else {
                return kukac = 2, pont = 1;
            }
    } else {
        return kukac = 2, pont = 1;
    }

    return 0;
}

int main(int argc, char *argv[]){
    if(argc != 2){
        printf("Hasznalat: %s e-mail cim ", argv[0]);
    } else {
        char **cim = &argv[0];
        ValidEmail(cim);
    }

    if(kukac < pont){
        printf("Valos e-mail cim!");
    } else {
        printf("Nem valos e-mail, kerem adjon meg valos e-mailt!");
    }

    return 0;
}
